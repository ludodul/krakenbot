# Krakenbot

Here is a krakenbot project with python language.  
Volumes and thresholds auto adapt with not filled orders.  
Write all orders filled and not filled in files.csv readable with pandas.

## Installation

python virtual env:

```
python3 -m venv env
source env/bin/activate
pip install -r pip-requirements.txt
```

## Launch Bot

This bot is for now on notebook for being user friendly.
A Tkinter version will be up as soon as I will have time for it.

```
jupyter notebook
```
You need to have a __kraken.key__ file with your API keys as said in https://github.com/veox/python3-krakenex
__Never give them to anyone!!!!!__


## Available Pairs 

All pairs are available

## For windows users

You can download botV2.ipynb, tools.py and manually install all modules required in pip-requirements.txt.
Download and install Anaconda on https://www.anaconda.com/products/individual , checking "ADD TO PATH" box during installation, can help.

## Finally

I accept any remark or suggestion to help me increasing this bot

You can tip me if you want 

Eth addr: 0xd684Ea40D59Dd82c1D8f30e5CEA5424f0D426E66

Cro addr: 0x73Ec59D4Ee37287859C7E9DaD9D382295362f7fA





 
 