'''
Version pour capitaliser en crypto et non plus en euros
'''

import krakenex
import time
import numpy as np
import sys
from IPython.display import clear_output
import math
import pandas as pd
import os
import datetime

k = krakenex.API()
k.load_key('kraken.key')

R = k.query_public('AssetPairs')
PAIRS = list(R['result'].keys())

def get_more_trades_old(pair, loops):
    trades_final = []
    since = int(datetime.datetime.timestamp(datetime.datetime.now()) - 1200)* 10**9
    for _ in range(loops):
        try:
            trades = k.query_public('Trades',data={'pair': pair,'since': since})['result']
            #print(trades["XETHZEUR"][0])
            last = trades['last']
            diff = int(int(last) - trades[pair][0][2]*1000000000)
            trades = trades[pair]
            trades_final = [float(trade[0]) for trade in trades] +trades_final
            since = f'{int(int(last) - 1.75*diff)}'
            
        except:
            res = None
            break
    res = trades_final
    return res

def get_more_trades(pair, loops):
    trades_final = []
    since = int(datetime.datetime.timestamp(datetime.datetime.now()) - 900*loops)* 10**9
    
    try:
        trades = k.query_public('Trades',data={'pair': pair,'since': since})['result']
        #print(trades["XETHZEUR"][0])
        trades = trades[pair]
        trades_final = [float(trade[0]) for trade in trades] +trades_final
                
    except:
        res = None
        
    res = trades_final
    return res


def order_type(pair,delta = None,**filters):
    """
    the keys pair and type are needed in filters
    """
    if len(pair) == 8:
        filters['pair'] = pair[1:4] + pair[5:8]
    else:
        filters['pair']= pair
    #print(filters)
    continuer = True
    while continuer:
        try:
            orders = k.query_private('OpenOrders',data = {'pair': pair})['result'].get('open')
            continuer = False
            time.sleep(3)
        except:
            print("Unexpected error:", sys.exc_info()[0])
            
            time.sleep(3)
    keys = list(orders.keys())
    
    supp_delta = len([orders[order] for order in list(orders.keys()) if (orders[order]['descr']['type'] == filters['type'] and orders[order]['descr']['pair'] == filters['pair'])])
    print("delta : ", delta + supp_delta)
    if orders != {} and filters:
        res = []
        date = int(datetime.datetime.timestamp(datetime.datetime.now()) - 1200)* 10**9
        r = k.query_public('Trades',data={'pair': pair, 'since': date})
        trades = r.get('result',{}).get(pair)
        actuel = float(r.get('result',{}).get(pair)[-1][0])

        #trades = k.query_public('Trades',data={'pair': pair}).get('result',{}).get(pair)
        #actuel = float(trades[-1][0])
        print('actuel : ', actuel)
        for order in list(orders.keys()):
            match = True
            for filtre in list(filters.keys()):                
                if orders[order]['descr'][filtre] != filters[filtre]:
                    match = False
                    break
            
            if delta and match:
                order_price = float(orders[order]['descr']['price'])
                print('order_price :', order_price)
                #trades = k.query_public('Trades',data={'pair': pair}).get('result',{}).get(pair)
                if trades:
                    #actuel = float(trades[-1][0])
                    #print('actuel : ', actuel)
                    diff = abs(actuel-order_price)/order_price * 100
                    print("diff ---------------> ",diff)
                    #time.sleep(1)
                    if 100 > diff > delta + supp_delta:
                        match = False
                    
            if match:
                res.append({order: orders[order]}) 
    else:
        res = orders    
    return res,supp_delta,actuel

def launch_trade(pair, loops, delta, volume_buysell, volume_sellbuy, rate_buysell, rate_sellbuy, limit_rate_buy, limit_rate_sell,rounded):
    if pair not in PAIRS:
        assert False, 'pair is not available pair'
    csv_path = f'{pair}.csv'
    if not os.path.exists(csv_path):
        open(csv_path,'w')
        
    try:
        df = pd.read_csv(csv_path)                    
    except:
        df = pd.DataFrame(
            columns=[
                'order_id',
                'price',
                'type',
                'filled',
                'volume'
            ]
        )
        df.to_csv(csv_path,index=False)
    rounds = 0
    supp_delta_buy = 0
    supp_delta_sell = 0
    while True:
        rounds += 1
        if rounds % 5 == 0:
            clear_output(wait=True)
        #print(rounds)
        filters = {
            'type': 'sell'
        }
        continuer = True
        while continuer:
            try:
                orders, supp_delta_buy, actuel_buy = order_type(pair,delta = delta,**filters)
                volume_buysell_t = round(volume_buysell*(1+supp_delta_sell/10),6)
                print('volume_buysell: ', volume_buysell_t)
                continuer = False
            except:
                print("Unexpected error:", sys.exc_info()[0])
                print('failed')
                time.sleep(30)
        
        if orders == [] and supp_delta_buy < 7:
            trades = get_more_trades(pair,loops)
            
            if trades:
                #maxi = np.max(trades[:(loops-1)*1000+600])
                maxi = np.max(trades)
                #actuel = trades[-1]
                actuel = actuel_buy
                #mini = np.min(trades[:(loops-1)*1000+600])
                mini = np.min(trades)
                rate_buy = -(maxi - actuel)/maxi * 100
                #rate_sell = -(mini - actuel)/mini * 100
                print('buy',maxi,actuel,rate_buy)
                #print(mini,actuel,rate_sell)
                rate_delta_sell = math.exp(-supp_delta_sell/8.5)  if math.exp(-supp_delta_sell/8.5)>0.45 else 0.45
                print('limit rate: ',limit_rate_buy*rate_delta_sell)
                if rate_buy < limit_rate_buy*rate_delta_sell:
                    r = k.query_private(
                    'AddOrder',
                    {
                        'pair': pair,
                        'type': 'buy',
                        'ordertype': 'limit',
                        'price': f'{actuel}',
                        'volume': f'{volume_buysell_t}'
                    })
                    
                    
                    time.sleep(20)
                    try:
                        open_order = r['result']['txid'][0]
                    except:
                        print('ordre échoué!')
                        open_order = None
                    continuer = True
                    while continuer:
                        try:
                            orders = k.query_private('OpenOrders')['result']['open']
                            continuer = False
                        except:
                            print("Unexpected error:", sys.exc_info()[0])            
                            time.sleep(3)
                    df = pd.read_csv(csv_path)
                    not_filled = list(df[df["filled"]=='no']['order_id'])
                    for order_id in not_filled:
                        if not order_id in orders:
                            ind = df.index[df["order_id"] == order_id][0]
                            new_line = [ value if value != 'no' else 'yes' for value in df.iloc[ind] ]
                            df.iloc[ind] = new_line
                    
                    df.to_csv(csv_path,index=False)       
                    time.sleep(10)
                    count = 0
                    while (open_order in orders) and count < 20:
                        try:
                            orders = k.query_private('OpenOrders')['result']['open']
                            print('ordre non transigé')
                            count += 1
                            time.sleep(5)
                        except:
                            print('failed')
                    continuer = True
                    while continuer:
                        try:
                            orders = k.query_private('OpenOrders')['result']['open']
                            continuer = False
                        except:
                            print("Unexpected error:", sys.exc_info()[0])            
                            time.sleep(3)
                    if open_order in orders:
                        k.query_private(
                            'CancelOrder',
                            {
                                'txid': open_order
                        })
                    elif open_order:
                        df = df.append({
                            "order_id":f'{open_order}',
                            "price": f'{actuel}',
                            "type":"buy",
                            "filled": 'yes',
                            "volume": f'{volume_buysell_t}'
                        },ignore_index=True)
                        r = k.query_private(
                            'AddOrder',
                            {
                                'pair': pair,
                                'type': 'sell',
                                'ordertype': 'limit',
                                'price': f'{round(actuel * (1.004 + rate_buysell/100),rounded)}',   # 1.004 is for fees ...
                                'volume': f'{round(volume_buysell_t/(1 + rate_buysell/100),4)}'
                        })
                        try:
                            open_order = r['result']['txid'][0]
                        
                            df = df.append({
                                "order_id":f'{open_order}',
                                "price": f'{round(actuel * (1.004 + rate_buysell/100),rounded)}',
                                "type":"sell",
                                "filled": 'no',
                                "volume": f'{round(volume_buysell_t/(1 + rate_buysell/100),4)}'
                            },ignore_index=True)
                            df.to_csv(csv_path,index=False)
                        except:
                            print('ordre échoué!')
                    time.sleep(20)
        else:
            print('buy order already done')
        filters = {
            'type': 'buy'
        }
        time.sleep(5)
        continuer = True
        while continuer:
            try:
                orders, supp_delta_sell,actuel_sell = order_type(pair,delta = delta,**filters)
                volume_sellbuy_t = round(volume_sellbuy*(1+supp_delta_buy/10),6)
                print('volume_sellbuy: ', volume_sellbuy_t)
                continuer = False
            except:
                print('failed')
                time.sleep(3)
        #print("orders", orders)
        if orders == [] and supp_delta_sell<7:
            trades = get_more_trades(pair,loops)
            if trades:
                #maxi = np.max(trades[:(loops-1)*1000+600])
                maxi = np.max(trades)
                #actuel = trades[-1]
                actuel = actuel_sell
                #mini = np.min(trades[:(loops-1)*1000+600])
                mini = np.min(trades)
                #rate_buy = -(maxi - actuel)/maxi * 100
                rate_sell = -(mini - actuel)/mini * 100
                #print('sell',maxi,actuel,rate)
                print('sell',mini,actuel,rate_sell)
                rate_delta_buy = math.exp(-supp_delta_buy/8.5)  if math.exp(-supp_delta_buy/8.5)>0.45 else 0.45
                print('limit rate: ',limit_rate_sell*rate_delta_buy)
                if rate_sell > limit_rate_sell*rate_delta_buy:
                    r = k.query_private(
                    'AddOrder',
                    {
                        'pair': pair,
                        'type': 'sell',
                        'ordertype': 'limit',
                        'price': f'{actuel}',
                        'volume': f'{volume_sellbuy_t}'
                    })
                    time.sleep(20)
                    try:
                        open_order = r['result']['txid'][0]
                    except:
                        print('ordre échoué!')
                        open_order = None
                    continuer = True
                    while continuer:
                        try:
                            orders = k.query_private('OpenOrders')['result']['open']
                            continuer = False
                        except:
                            print("Unexpected error:", sys.exc_info()[0])            
                            time.sleep(3)
                    df = pd.read_csv(csv_path)
                    not_filled = list(df[df["filled"]=='no']['order_id'])
                    for order_id in not_filled:
                        if not order_id in orders:
                            ind = df.index[df["order_id"] == order_id][0]
                            new_line = [ value if value != 'no' else 'yes' for value in df.iloc[ind] ]
                            df.iloc[ind] = new_line
                    
                    df.to_csv(csv_path,index=False)
                    time.sleep(10)
                    count = 0
                    while (open_order in orders) and count < 20:
                        try:
                            orders = k.query_private('OpenOrders')['result']['open']
                            print('ordre non transigé')
                            count += 1
                            time.sleep(5)
                        except:
                            print("failed")
                    continuer = True
                    while continuer:
                        try:
                            orders = k.query_private('OpenOrders')['result']['open']
                            continuer = False
                        except:
                            print("Unexpected error:", sys.exc_info()[0])            
                            time.sleep(3)
                    
                    if open_order in orders:
                        k.query_private(
                            'CancelOrder',
                            {
                                'txid': open_order
                        })
                    elif open_order:
                        df = df.append({
                            "order_id":f'{open_order}',
                            "price": f'{actuel}',
                            "type":"sell",
                            "filled": 'yes',
                            "volume": f'{volume_sellbuy_t}'
                        },ignore_index=True)
                        r = k.query_private(
                            'AddOrder',
                            {
                                'pair': pair,
                                'type': 'buy',
                                'ordertype': 'limit',
                                'price': f'{round(actuel * (0.996 - rate_sellbuy/100),rounded)}',
                                'volume': f'{round(volume_sellbuy_t/ (1 - rate_sellbuy/100),5)}'
                        })
                        try:
                            open_order = r['result']['txid'][0]
                        except:
                            print(r)
                            assert False
                        df = df.append({
                            "order_id":f'{open_order}',
                            "price": f'{round(actuel * (0.996 - rate_sellbuy/100),rounded)}',
                            "type":"buy",
                            "filled": 'no',
                            "volume": f'{round(volume_sellbuy_t/ (1 - rate_sellbuy/100),5)}'
                        },ignore_index=True)
                        df.to_csv(csv_path,index=False)
                    time.sleep(20)
        else:
            print('sell order already done')        
                    
        time.sleep(30)