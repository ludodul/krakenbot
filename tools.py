import krakenex
import time
import numpy as np
import sys

k = krakenex.API()
k.load_key('kraken.key')

R = k.query_public('AssetPairs')
PAIRS = list(R['result'].keys())

def get_more_trades(pair, loops):
    trades_final = []
    since = None
    for _ in range(loops):
        try:
            trades = k.query_public('Trades',data={'pair': pair,'since': since})['result']
            #print(trades["XETHZEUR"][0])
            last = trades['last']
            diff = int(int(last) - trades[pair][0][2]*1000000000)
            trades = trades[pair]
            trades_final = [float(trade[0]) for trade in trades] +trades_final
            since = f'{int(int(last) - 1.75*diff)}'
            
        except:
            res = None
            break
    res = trades_final
    return res


def order_type(pair,delta = None,**filters):
    """
    the keys pair and type are needed in filters
    """
    if len(pair) == 8:
        filters['pair'] = pair[1:4] + pair[5:8]
    #print(filters)
    continuer = True
    while continuer:
        try:
            orders = k.query_private('OpenOrders',data = {'pair': pair})['result'].get('open')
            continuer = False
            time.sleep(3)
        except:
            print("Unexpected error:", sys.exc_info()[0])
            
            time.sleep(3)
    keys = list(orders.keys())
    supp_delta = len([orders[order] for order in list(orders.keys()) if (orders[order]['descr']['type'] == filters['type'] and orders[order]['descr']['pair'] == filters['pair'])])
    print("delta : ", delta + supp_delta)
    if orders != {} and filters:
        res = []
        
        for order in list(orders.keys()):
            match = True
            for filtre in list(filters.keys()):                
                if orders[order]['descr'][filtre] != filters[filtre]:
                    match = False
                    break
            
            if delta and match:
                order_price = float(orders[order]['descr']['price'])
                trades = k.query_public('Trades',data={'pair': pair}).get('result',{}).get(pair)
                if trades:
                    actuel = float(trades[-1][0])
                    diff = abs(actuel-order_price)/order_price * 100
                    print("diff ---------------> ",diff)
                    if diff > delta + supp_delta:
                        match = False
                    
            if match:
                res.append({order: orders[order]}) 
    else:
        res = orders    
    return res,supp_delta