from flask import Flask
import datetime



app = Flask(__name__)

DATE = ""

@app.route('/')
def hello_world():
    global DATE
    date = datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
    DATE = date
    return date

@app.route('/test')
def check():
    return DATE

if __name__ == "__main__":
    app.run(host='0.0.0.0')